// USES

use std::io::Result;            // main() load()
use std::env::args;             // main()
use std::{thread, time};        // main()
use std::fs::read_to_string;    // load()
use rand::Rng;                  // exec()
use std::fmt;                   // impl
//use ansi_term::Colour;        // Pour les couleurs dans le terminal, mais je n'ai pas besoin de l'utiliser

// STRUCTS

struct Color {
    x: u32,             // x coordinate
    y: u32,             // y coordinate

}

//#[derive(Debug)]
struct Robot {
    x: u32,             // x coordinate
    y: u32,             // y coordinate
    o: String,          // orientation
    p: String,          // program
    c: Vec<Color>       // color trace
}

struct Obstacle {
    x: u32,             // x coordinate
    y: u32,             // y coordinate
    c: bool,             // collision
    i: bool             // impacted
}

struct Game {
    w: u32,             // width
    h: u32,             // height
    r: Vec<Robot>,      // robots
    o: Vec<Obstacle>    // obstacles
}

// IMPLEMENTS

impl fmt::Display for Game {
    fn fmt(&self, _: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.h+1 {
            for x in 0..self.w+1 {
                if y!= self.h {
                    if x==0 {
                        println!("");
                        print!("{} ",&(self.h-y-1).to_string());
                    } else {
                        let mut c = '⬛';
                        for r in &self.r {
                            if r.x==x-1 && r.y==self.h-y-1 {
                            
                            // A partir d'ici et avec ansi_term::Colour, je peux gerer
                            // l'affichage en couleur sur le terminal, mais je prefere
                            // troller avec des emoji trotinete electrique XD

                                if r.p.is_empty() {
                                    c = '🛴';
                                } else if r.o == "N".to_string() {
                                    c = '🛴';
                                } else if r.o == "E".to_string() {
                                    c = '🛴';
                                } else if r.o == "S".to_string() {
                                    c = '🛴';
                                } else if r.o == "W".to_string() {
                                    c = '🛴';
                                }
                            } else if r.c[0].x==x-1 && r.c[0].y==self.h-y-1 {
                                c = '🔥';
                            } else if r.c[1].x==x-1 && r.c[1].y==self.h-y-1 {
                                c = '🔥';
                            } else if r.c[2].x==x-1 && r.c[2].y==self.h-y-1 {
                                c = '🔥';
                            } else if r.c[3].x==x-1 && r.c[3].y==self.h-y-1 {
                                c = '🔥';
                            } else if r.c[4].x==x-1 && r.c[4].y==self.h-y-1 {
                                c = '🔥';
                            }
                        }
                        for o in &self.o {
                            if o.x==x-1 && o.y==self.h-y-1 {
                                if o.i {
                                    print!("{}","⚰");
                                    c = ' '; 
                                } else {
                                    c = '🚶'; 
                                }
                            }
                        }
                        print!("{}",c);
                    }
                } else {
                    if x==0 {
                        print!("\n  ");
                    } else {
                        print!(" {}", &(x-1).to_string());
                    }
                }
            }
        }
        println!("");
        return Ok(());
    }
}

trait Throw {
    fn throw(self) -> Self;
}

impl Throw for String {
    fn throw(self) -> Self {
        let empty = String::from("");
        empty
    }
}

// PROGRAM FUNCTIONS

fn main() -> Result<()> {

    // Creation de la partie
    let mut game = Game {
        w:0,
        h:0,
        r:Vec::new(),
        o:Vec::new()
    };

    // Recuperation du nom du fichier
    let mut args:Vec<String> = args().collect();
    if args.len() < 2 {
        args.push(String::from("../two_robots.txt"));
    }

    // Chargement du fichier
    load(&args[1], &mut game)?;

    // Execution du programme et simple affichage
    println!("Map : width = {}; height = {}", game.w+1, game.h+1);
    loop {

        // Execution
        exec(&mut game);

        // Affichage
        print!("{}",game);

        // Fin de Loop
        let mut stop = true;
        for r in &game.r {
            if r.p!="" {
               stop = false;
            }
        }
        if stop {
            break;
        }

        // Latence
        thread::sleep(time::Duration::from_millis(50));
    }

    Ok(())

}

fn load(name:&String, game:&mut Game) -> Result<()>  {
    
    // Copie du fichier dans la memoire
    let file = read_to_string(name).expect("File not found");
    let mut text = file.lines();

    // Recuperation du header
    let mut head = text.next().expect("Empty file").split_whitespace();
    game.w = head.next().expect("Map width not found").parse::<u32>().expect("Invalid map width");
    game.h = head.next().expect("Map height not found").parse::<u32>().expect("Invalid map height");
    game.w += 1;
    game.h += 1;

    // Depot des obtacles au hasard
    let mut rng = rand::thread_rng();
    for _ in 0..rng.gen_range(0, game.w+game.h) {
        game.o.push(Obstacle {
            x:rng.gen_range(0, game.w),
            y:rng.gen_range(0, game.h),
            c:false, // Pour que les trotinettes ecrasent les pasants
            i:false  // Parce que les passants sont vivants avant de se faire ecraser
        });
    }

    // Recuperation des robots
    while let Some(line) = text.next() {

        // Ignore les lignes vides avant la creation d'un nouveau robot
        if line.chars().next().is_none() {
            continue;
        }

        // Enregistre la position et l'orientation du nouveau robot
        let mut coor = line.split_whitespace();
        let x = coor.next().expect("Robot x position not found").parse::<u32>().expect("Invalid robot x position");
        let y = coor.next().expect("Robot y position not found").parse::<u32>().expect("Invalid robot y position");
        let o = match coor.next().expect("Robot orientation not found") {
            c if c=="N" || c=="E" || c=="S" || c=="W" => String::from(c),
            _  => panic!("Invalid robot orientation")
        };

        /* J'aurai pu prendre la décision d'ignorer encore une fois les lignes vides,
           mais l'énoncé du projet semble dire que la ligne decrivant la position du
           robot et celle decrivant son mouvement doivent forcement se suivre. */

        // Enregistre la liste de commandes du nouveau robot
        let next = text.next().expect("Robot program not found");
        let prog = next.split_whitespace().next().expect("Empty line after robot coordinate");
        for c in prog.chars() {
            match c {
                'L' | 'F' | 'R' => {}
                       _        => panic!("Invalid robot program")
            }
        }

        //Enregistre les tracé de couleur
        let mut c = Vec::new();
        for _ in 0..5 {
            c.push(Color {
                x,
                y
            });
        }

        // Reporte toutes les valeurs dans un nouveau robot et le push
        game.r.push(Robot {
            x,
            y,
            o,
            p:prog.to_string(),
            c
        })

    }

    Ok(())

}

fn exec(game:&mut Game) {

    // Choisi un robot au hasard
    let mut rng = rand::thread_rng();
    let i = rng.gen_range(0, game.r.len());

    // Actualise le tracé de couleur
    for j in 1..5 {
        game.r[i].c[j] = Color {
            x:game.r[i].c[j-1].x,
            y:game.r[i].c[j-1].y
        };
    }
    game.r[i].c[0] = Color {
        x:game.r[i].x,
        y:game.r[i].y
    };

    // Execute l'instruction
    let code = game.r[i].p.chars().next();
    game.r[i].p = game.r[i].p.chars().skip(1).collect();
    match code {
        l if l==Some('L') => game.r[i].o = match &game.r[i].o {
            n if n==&"N".to_string() => 'W'.to_string(),
            e if e==&"E".to_string() => 'N'.to_string(),
            s if s==&"S".to_string() => 'E'.to_string(),
            w if w==&"W".to_string() => 'S'.to_string(),
            x => x.to_string()
        },
        r if r==Some('R') => game.r[i].o = match &game.r[i].o {
            n if n==&"N".to_string() => 'E'.to_string(),
            e if e==&"E".to_string() => 'S'.to_string(),
            s if s==&"S".to_string() => 'W'.to_string(),
            w if w==&"W".to_string() => 'N'.to_string(),
            x => x.to_string()
        },
        f if f==Some('F') => match &game.r[i].o {
            n if n==&"N".to_string() => step(game, i, game.r[i].x as i32, (game.r[i].y as i32)+1),
            e if e==&"E".to_string() => step(game, i, (game.r[i].x as i32)+1, game.r[i].y as i32),
            s if s==&"S".to_string() => step(game, i, game.r[i].x as i32, (game.r[i].y as i32)-1),
            w if w==&"W".to_string() => step(game, i, (game.r[i].x as i32)-1, game.r[i].y as i32),
            _ => ()
       },
       _ => {}
    }
    
}

// Tente un deplacement
fn step(game:&mut Game, i:usize, x_u:i32, y_u:i32) {

    // Empeche la sortie du robot de la carte
    if x_u<0 || x_u>=(game.w as i32) || y_u<0 || y_u>=(game.h as i32) {
        return;
    }
    let x = x_u as u32;
    let y = y_u as u32;

    // Empeche la collision de deux robots
    let mut id = 0;
    let mut collision = false;
    for r in &game.r {
        if r.x==x && r.y==y {
            collision = true;
            print!("Robot ID{} Collision en ({},{})\n",id,x,y);
        } else {
            id += 1;
        }
    }
    if collision {
        game.r[i].p = "".to_string();
    }

    // Empeche la collision du robot avec un obtacle
    let mut impacted = false;
    let mut j = 0;
    for o in &game.o {
        if o.x==x && o.y==y {
            if o.c {
                return;
            } else {
                impacted = true;
                break;
            }
        }
        j+=1;
    }
    if impacted {
        game.o[j].i = true;
    }

    game.r[i].x = x;
    game.r[i].y = y;

}